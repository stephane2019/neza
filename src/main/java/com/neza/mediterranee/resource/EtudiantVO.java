package com.neza.mediterranee.resource;

/*
 @author STEPHANE HOZIGRE
 @since 01/11/2020
*/

import lombok.Builder;

import java.util.Date;

@Builder
public class EtudiantVO {

    private int id;

    private String name;

    private Date createdDate;

    public EtudiantVO(int id, String name, Date createdDate) {
        this.id = id;
        this.name = name;
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
