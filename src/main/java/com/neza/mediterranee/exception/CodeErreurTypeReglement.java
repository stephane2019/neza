package com.neza.mediterranee.exception;

import com.neza.mediterranee.repository.utils.exception.HasCodeErreur;

public enum CodeErreurTypeReglement implements HasCodeErreur {
    DOUBLON_SUR_ENTITE(3100L);

    /**
     * Valeur minimale des codes de cette application.
     */
    public static final int CODE_MIN = 3000;

    /**
     * Valeur maximale des codes de cette application.
     */
    public static final int CODE_MAX = 3999;

    private final Long code;

    /**
     * Crée un code erreur.
     *
     * @param code le code de l'erreur.
     */
    CodeErreurTypeReglement(Long code) {
        if (code < CODE_MIN || code >= CODE_MAX) {
            throw new IllegalStateException(String.format("Le code erreur doit se trouver entre %s et %s", CODE_MIN, CODE_MAX));
        }
        this.code = code;
    }

    @Override
    public Long getCode() {
        return code;
    }
}
