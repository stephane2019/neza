package com.neza.mediterranee.exception;

import com.neza.mediterranee.repository.utils.exception.AbstractApplicationException;
import com.neza.mediterranee.repository.utils.exception.CodeErreurTechnique;
import com.neza.mediterranee.repository.utils.exception.HasCodeErreur;

/**
 * Exception concernant les dons.
 */
public class TypeEmballageException extends AbstractApplicationException {

    public TypeEmballageException(HasCodeErreur codeErreur, String cleTraduction, Object... params) {
        super(codeErreur, cleTraduction, params);
    }

    public static TypeEmballageException doublonSurEntiteException() {
        return new TypeEmballageException(CodeErreurTypeEmballage.DOUBLON_SUR_ENTITE, "Un doublon a été détecté");
    }
}
