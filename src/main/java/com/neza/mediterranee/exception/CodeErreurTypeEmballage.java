package com.neza.mediterranee.exception;

import com.neza.mediterranee.repository.utils.exception.HasCodeErreur;

public enum CodeErreurTypeEmballage implements HasCodeErreur {
    DON_EXISTANT_POUR_BENEFICIAIRE(3000L),
    IMPORT_SOCIETE_IMPOSSIBLE(3001L),
    CODE_SOCIETE_MANQUANT(3002L),
    RECAPITULATIF_EXISTANT_POUR_DON(3003L),
    REPORT_GENERATE_ERROR(3003L),
    ENCODAGE_INCONNU(3004L),
    AUCUN_RECAP_POUR_ATTESTATION(3005L),
    ATTESTATION_DEJA_ENVOYEE(3006L),
    BENEFICIAIRE_BLOQUE(3007L),
    DON_UTILISE_POUR_RECAPITULATIF(3008L),
    AUCUNE_LIAISON_BENEFICIAIRE_SOCIETE(3009L),
    DOUBLON_SUR_ENTITE(3100L);

    /**
     * Valeur minimale des codes de cette application.
     */
    public static final int CODE_MIN = 3000;

    /**
     * Valeur maximale des codes de cette application.
     */
    public static final int CODE_MAX = 3999;

    private final Long code;

    /**
     * Crée un code erreur.
     *
     * @param code le code de l'erreur.
     */
    CodeErreurTypeEmballage(Long code) {
        if (code < CODE_MIN || code >= CODE_MAX) {
            throw new IllegalStateException(String.format("Le code erreur doit se trouver entre %s et %s", CODE_MIN, CODE_MAX));
        }
        this.code = code;
    }

    @Override
    public Long getCode() {
        return code;
    }
}
