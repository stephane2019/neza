package com.neza.mediterranee.exception;

import com.neza.mediterranee.repository.utils.exception.AbstractApplicationException;
import com.neza.mediterranee.repository.utils.exception.HasCodeErreur;

/**
 * Exception concernant les types reglements.
 */
public class TypeMagasinException extends AbstractApplicationException {

    public TypeMagasinException(HasCodeErreur codeErreur, String cleTraduction, Object... params) {
        super(codeErreur, cleTraduction, params);
    }

    public static TypeMagasinException doublonSurEntiteException() {
        return new TypeMagasinException(CodeErreurTypeMagasin.DOUBLON_SUR_ENTITE, "Un doublon a été détecté");
    }
}

