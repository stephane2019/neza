package com.neza.mediterranee.exception;

import com.neza.mediterranee.repository.utils.exception.AbstractApplicationException;
import com.neza.mediterranee.repository.utils.exception.HasCodeErreur;

/**
 * Exception concernant les types reglements.
 */
public class TypeReglementException extends AbstractApplicationException {

    public TypeReglementException(HasCodeErreur codeErreur, String cleTraduction, Object... params) {
        super(codeErreur, cleTraduction, params);
    }

    public static TypeReglementException doublonSurEntiteException() {
        return new TypeReglementException(CodeErreurTypeReglement.DOUBLON_SUR_ENTITE, "Un doublon a été détecté");
    }
}
