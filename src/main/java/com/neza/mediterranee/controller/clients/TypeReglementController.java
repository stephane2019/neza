package com.neza.mediterranee.controller.clients;

import javax.validation.Valid;

import com.neza.mediterranee.facades.clients.TypeReglementFacade;
import com.neza.mediterranee.presentation.vo.clients.TypeReglementVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ws/typeReglement")
public class TypeReglementController {

    private final TypeReglementFacade typeReglementFacade;

    public TypeReglementController(TypeReglementFacade typeReglementFacade) {
        this.typeReglementFacade = typeReglementFacade;
    }

    /**
     * Créé un nouveau type reglement.
     *
     * @param typeReglementVO les données du type reglement à créer.
     */
    @PostMapping
    public void creerTypeReglement(@RequestBody @Valid TypeReglementVO typeReglementVO) {
        typeReglementFacade.creerTypeReglement(typeReglementVO);
    }

    /**
     * Récupère un type reglement selon son code.
     *
     * @param code le code du type reglement à récupérer.
     */
    @GetMapping("{code}")
    public TypeReglementVO recupererTypeReglement(@PathVariable String code) {
        return typeReglementFacade.recupererTypeReglement(code);
    }

    /**
     * Modifie un type reglement.
     *
     * @param typeReglementVO le VO contenant les informations du type reglement.
     */
    @PutMapping("{code}")
    public void modifierTypeReglement(@RequestBody @Valid TypeReglementVO typeReglementVO) {
        typeReglementFacade.modifierTypeReglement(typeReglementVO);
    }

    /**
     * Récupère l'ensemble des types regelements suivants les critéres de recherche.
     *
     * @param code le code du type reglement.
     * @param designation la designation du type reglement.
     * @return la liste de VOs des types reglements.
     */
    @GetMapping
    public List<TypeReglementVO> listerTypeReglements(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String designation) {
        return typeReglementFacade.listerTypeReglements(code, designation);
    }

    /**
     * Supprime un type regelement.
     *
     * @param code le code du type reglement.
     */
    @DeleteMapping(value = "/{code}")
    public void supprimerTypeReglement(@PathVariable String code) {
        typeReglementFacade.supprimerTypeReglement(code);
    }
}

