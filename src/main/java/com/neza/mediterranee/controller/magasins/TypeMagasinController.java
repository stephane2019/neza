package com.neza.mediterranee.controller.magasins;

import javax.validation.Valid;

import com.neza.mediterranee.facades.magasins.TypeMagasinFacade;
import com.neza.mediterranee.presentation.vo.magasins.TypeMagasinVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ws/typeMagasin")
public class TypeMagasinController {

    private final TypeMagasinFacade typeMagasinFacade;

    public TypeMagasinController(TypeMagasinFacade typeMagasinFacade) {
        this.typeMagasinFacade = typeMagasinFacade;
    }

    /**
     * Créé un nouveau type magasin.
     *
     * @param typeMagasinVO les données du type magasin à créer.
     */
    @PostMapping
    public void creerTypeMgasin(@RequestBody @Valid TypeMagasinVO typeMagasinVO) {
        typeMagasinFacade.creerTypeMagasin(typeMagasinVO);
    }

    /**
     * Récupère un type magasin selon son code.
     *
     * @param code le code du type magasin à récupérer.
     */
    @GetMapping("{code}")
    public TypeMagasinVO recupererTypeMagasin(@PathVariable String code) {
        return typeMagasinFacade.recupererTypeMagasin(code);
    }

    /**
     * Modifie un type magasin.
     *
     * @param typeMagasinVO le VO contenant les informations du type magasin.
     */
    @PutMapping("{code}")
    public void modifierTypeReglement(@RequestBody @Valid TypeMagasinVO typeMagasinVO) {
        typeMagasinFacade.modifierTypeMagasin(typeMagasinVO);
    }

    /**
     * Récupère l'ensemble des types magasins suivants les critéres de recherche.
     *
     * @param code le code du type magasin.
     * @param designation la designation du type magasin.
     * @return la liste de VOs des types magasins.
     */
    @GetMapping
    public List<TypeMagasinVO> listerTypeMagasins(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String designation) {
        return typeMagasinFacade.listerTypeMagasins(code, designation);
    }

    /**
     * Supprime un type magasin.
     *
     * @param code le code du type magasin.
     */
    @DeleteMapping(value = "/{code}")
    public void supprimerTypeMagasin(@PathVariable String code) {
        typeMagasinFacade.supprimerTypeMagasin(code);
    }
}


