package com.neza.mediterranee.controller.emballages;

import javax.validation.Valid;

import com.neza.mediterranee.facades.emballages.TypeEmballageFacade;
import com.neza.mediterranee.presentation.vo.emballages.TypeEmballageVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ws/typeEmballage")
public class TypeEmballageController {

    private final TypeEmballageFacade typeEmballageFacade;

    public TypeEmballageController(TypeEmballageFacade typeEmballageFacade) {
        this.typeEmballageFacade = typeEmballageFacade;
    }

    /**
     * Créé un nouveau type d'embalage
     *
     * @param typeEmballageVO les données du type d'emballage à créer.
     */
    @PostMapping
    public void creerTypeEmballage(@RequestBody @Valid TypeEmballageVO typeEmballageVO) {
        typeEmballageFacade.creerTypeEmballage(typeEmballageVO);
    }

    /**
     * Récupère un type emballage selon son code.
     *
     * @param code le code du type emballage à récupérer.
     */
    @GetMapping("{code}")
    public TypeEmballageVO recupererTypeEmballage(@PathVariable String code) {
        return typeEmballageFacade.recupererTypeEmballage(code);
    }

    /**
     * Modifie un type emballage.
     *
     * @param typeEmballageVO le VO contenant les informations du type emballage.
     */
    @PutMapping("{code}")
    public void modifierTypeEmballage(@RequestBody @Valid TypeEmballageVO typeEmballageVO) {
        typeEmballageFacade.modifierTypeEmballage(typeEmballageVO);
    }

    /**
     * Récupère l'ensemble des types emballages suivants les critéres de recherche.
     *
     * @param code le code du type emballage.
     * @param designation la designation du type emballage.
     * @return la liste de VOs des types emballages.
     */
    @GetMapping
    public List<TypeEmballageVO> listerTypeEmballages(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String designation) {
        return typeEmballageFacade.listerTypeEmballages(code, designation);
    }

    /**
     * Supprime un type emballage.
     *
     * @param code le code du type emballage.
     */
    @DeleteMapping(value = "/{code}")
    public void supprimerTypeEmballage(@PathVariable String code) {
        typeEmballageFacade.supprimerTypeEmballage(code);
    }
}
