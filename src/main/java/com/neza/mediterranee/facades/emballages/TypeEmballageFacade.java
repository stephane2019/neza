package com.neza.mediterranee.facades.emballages;

import com.neza.mediterranee.domain.emballages.TypeEmballage;
import com.neza.mediterranee.presentation.factory.emballages.TypeEmballageVoFactory;
import com.neza.mediterranee.presentation.vo.emballages.TypeEmballageVO;
import com.neza.mediterranee.repository.emballages.TypeEmballageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.neza.mediterranee.exception.TypeEmballageException.doublonSurEntiteException;

@Service
public class TypeEmballageFacade {

    private final TypeEmballageRepository typeEmballageRepository;
    private final TypeEmballageVoFactory typeEmballageVoFactory;

    public TypeEmballageFacade(TypeEmballageRepository typeEmballageRepository, TypeEmballageVoFactory typeEmballageVoFactory) {
        this.typeEmballageRepository = typeEmballageRepository;
        this.typeEmballageVoFactory = typeEmballageVoFactory;
    }

    /**
     * Créé un nouveau type d'embalage
     *
     * @param typeEmballageVO les données du type d'emballage à créer.
     */
    @Transactional
    public void creerTypeEmballage(TypeEmballageVO typeEmballageVO) {
        String code = typeEmballageVO.getCode();
        String designation = typeEmballageVO.getDesignation();

        TypeEmballage resultat = typeEmballageRepository.rechercherParCode(code);
        if (resultat != null) throw doublonSurEntiteException();

        TypeEmballage typeEmballage = new TypeEmballage(code, designation);

        typeEmballageRepository.save(typeEmballage);
    }

    /**
     * Récupère un type emballage par son code.
     *
     * @param code le code du type emballage recherché.
     * @return le VO du type emballage.
     */
    @Transactional(readOnly = true)
    public TypeEmballageVO recupererTypeEmballage(String code) {
        return typeEmballageVoFactory.typeEmballageVO(typeEmballageRepository.rechercherParCode(code));
    }

    /**
     * Modifie un type emballage à partir des données reçues.
     *
     * @param typeEmballageVO les données du type emballage à modifier.
     */
    @Transactional
    public void modifierTypeEmballage(TypeEmballageVO typeEmballageVO) {
        String code = typeEmballageVO.getCode();
        String designation = typeEmballageVO.getDesignation();
        TypeEmballage typeEmballage = typeEmballageRepository.rechercherParCode(code);

        typeEmballage.renseignerTypeEmballage(code, designation);

        typeEmballageRepository.save(typeEmballage);
    }

    /**
     * Liste les types emballages en fonction des filtres.
     *
     * @param code le code du type emballage.
     * @param designation la designation du type emballage.
     * @return une liste de VO de type emballage.
     */
    @Transactional(readOnly = true)
    public List<TypeEmballageVO> listerTypeEmballages(String code, String designation) {
        List<TypeEmballage> typeEmballages = typeEmballageRepository.listerTypesEmballage(code, designation);

        return typeEmballageVoFactory.typeEmballagesVO(typeEmballages);
    }

    /**
     * Permet de supprimer un type emballage.
     *
     * @param code le code du type emballage.
     */
    @Transactional
    public void supprimerTypeEmballage(String code) {
        TypeEmballage typeEmballage = typeEmballageRepository.rechercherParCode(code);
        typeEmballageRepository.delete(typeEmballage);
    }
}
