package com.neza.mediterranee.facades.clients;

import com.neza.mediterranee.domain.clients.TypeReglement;
import com.neza.mediterranee.presentation.factory.clients.TypeReglementVoFactory;
import com.neza.mediterranee.presentation.vo.clients.TypeReglementVO;
import com.neza.mediterranee.repository.clients.TypeReglementRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.neza.mediterranee.exception.TypeReglementException.doublonSurEntiteException;

@Service
public class TypeReglementFacade {

    private final TypeReglementRepository typeReglementRepository;
    private final TypeReglementVoFactory typeReglementVoFactory;

    public TypeReglementFacade(TypeReglementRepository typeReglementRepository, TypeReglementVoFactory typeReglementVoFactory) {
        this.typeReglementRepository = typeReglementRepository;
        this.typeReglementVoFactory = typeReglementVoFactory;
    }

    /**
     * Créé un nouveau type reglement.
     *
     * @param typeReglementVO les données du type reglement à créer.
     */
    @Transactional
    public void creerTypeReglement(TypeReglementVO typeReglementVO) {
        String code = typeReglementVO.getCode();
        String designation = typeReglementVO.getDesignation();

        TypeReglement resultat = typeReglementRepository.rechercherParCode(code);
        if (resultat != null) throw doublonSurEntiteException();

        TypeReglement typeReglement = new TypeReglement(code, designation);

        typeReglementRepository.save(typeReglement);
    }

    /**
     * Récupère un type reglement par son code.
     *
     * @param code le code du type reglement recherché.
     * @return le VO du type reglement.
     */
    @Transactional(readOnly = true)
    public TypeReglementVO recupererTypeReglement(String code) {
        return typeReglementVoFactory.typeReglementVO(typeReglementRepository.rechercherParCode(code));
    }

    /**
     * Modifie un type emballage à partir des données reçues.
     *
     * @param typeReglementVO les données du type reglement à modifier.
     */
    @Transactional
    public void modifierTypeReglement(TypeReglementVO typeReglementVO) {
        String code = typeReglementVO.getCode();
        String designation = typeReglementVO.getDesignation();
        TypeReglement typeReglement = typeReglementRepository.rechercherParCode(code);

        typeReglement.renseignerTypeReglement(code, designation);

        typeReglementRepository.save(typeReglement);
    }

    /**
     * Liste les types reglements en fonction des filtres.
     *
     * @param code le code du type reglement.
     * @param designation la designation du type reglement.
     * @return une liste de VO de type reglement.
     */
    @Transactional(readOnly = true)
    public List<TypeReglementVO> listerTypeReglements(String code, String designation) {
        List<TypeReglement> typeReglements = typeReglementRepository.listerTypesReglements(code, designation);

        return typeReglementVoFactory.typesReglementsVO(typeReglements);
    }

    /**
     * Permet de supprimer un type reglement.
     *
     * @param code le code du type reglement.
     */
    @Transactional
    public void supprimerTypeReglement(String code) {
        TypeReglement typeReglement = typeReglementRepository.rechercherParCode(code);
        typeReglementRepository.delete(typeReglement);
    }
}
