package com.neza.mediterranee.facades.magasins;

import com.neza.mediterranee.domain.magasins.TypeMagasin;
import com.neza.mediterranee.presentation.factory.magasins.TypeMagasinVoFactory;
import com.neza.mediterranee.presentation.vo.magasins.TypeMagasinVO;
import com.neza.mediterranee.repository.magasins.TypeMagasinRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.neza.mediterranee.exception.TypeMagasinException.doublonSurEntiteException;

@Service
public class TypeMagasinFacade {

    private final TypeMagasinRepository typeMagasinRepository;
    private final TypeMagasinVoFactory typeMagasinVoFactory;

    public TypeMagasinFacade(TypeMagasinRepository typeMagasinRepository, TypeMagasinVoFactory typeMagasinVoFactory) {
        this.typeMagasinRepository = typeMagasinRepository;
        this.typeMagasinVoFactory = typeMagasinVoFactory;
    }

    /**
     * Créé un nouveau type d'embalage
     *
     * @param typeMagasinVO les données du type magasin à créer.
     */
    @Transactional
    public void creerTypeMagasin(TypeMagasinVO typeMagasinVO) {
        String code = typeMagasinVO.getCode();
        String designation = typeMagasinVO.getDesignation();

        TypeMagasin resultat = typeMagasinRepository.rechercherParCode(code);
        if (resultat != null) throw doublonSurEntiteException();

        TypeMagasin typeMagasin = new TypeMagasin(code, designation);

        typeMagasinRepository.save(typeMagasin);
    }

    /**
     * Récupère un type magasin par son code.
     *
     * @param code le code du type magasin recherché.
     * @return le VO du type emballage.
     */
    @Transactional(readOnly = true)
    public TypeMagasinVO recupererTypeMagasin(String code) {
        return typeMagasinVoFactory.typeMagasinVO(typeMagasinRepository.rechercherParCode(code));
    }

    /**
     * Modifie un type magasin à partir des données reçues.
     *
     * @param typeMagasinVO les données du type magasin à modifier.
     */
    @Transactional
    public void modifierTypeMagasin(TypeMagasinVO typeMagasinVO) {
        String code = typeMagasinVO.getCode();
        String designation = typeMagasinVO.getDesignation();
        TypeMagasin typeMagasin = typeMagasinRepository.rechercherParCode(code);

        typeMagasin.renseignerTypeMagasin(code, designation);

        typeMagasinRepository.save(typeMagasin);
    }

    /**
     * Liste les types magasins en fonction des filtres.
     *
     * @param code le code du type magasin.
     * @param designation la designation du type magasin.
     * @return une liste de VO de type magasin.
     */
    @Transactional(readOnly = true)
    public List<TypeMagasinVO> listerTypeMagasins(String code, String designation) {
        List<TypeMagasin> typeMagasins = typeMagasinRepository.listerTypesMagasins(code, designation);

        return typeMagasinVoFactory.typeMagasinsVO(typeMagasins);
    }

    /**
     * Permet de supprimer un type magasin.
     *
     * @param code le code du type magasin.
     */
    @Transactional
    public void supprimerTypeMagasin(String code) {
        TypeMagasin typeMagasin = typeMagasinRepository.rechercherParCode(code);
        typeMagasinRepository.delete(typeMagasin);
    }
}

