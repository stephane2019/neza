package com.neza.mediterranee.presentation.factory.magasins;

import com.neza.mediterranee.domain.emballages.TypeEmballage;
import com.neza.mediterranee.domain.magasins.TypeMagasin;
import com.neza.mediterranee.presentation.vo.emballages.TypeEmballageVO;
import com.neza.mediterranee.presentation.vo.magasins.TypeMagasinVO;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TypeMagasinVoFactory {

    /**
     * Convertir une liste de type magasin en une liste de VO de type magasin.
     *
     * @param typeMagasins les types magasins à convertir en VO.
     * @return la liste de VO des types emballages qui ont été transformé.
     */
    public List<TypeMagasinVO> typeMagasinsVO(List<TypeMagasin> typeMagasins) {
        return typeMagasins.stream()
                .map(TypeMagasinVO::new)
                .collect(toList());
    }

    /**
     *
     * @param typeMagasin le type magasin.
     * @return le VO du type magasin.
     */
    public TypeMagasinVO typeMagasinVO(TypeMagasin typeMagasin) {
        return new TypeMagasinVO(typeMagasin);
    }
}

