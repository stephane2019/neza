package com.neza.mediterranee.presentation.factory.emballages;

import com.neza.mediterranee.domain.emballages.TypeEmballage;
import com.neza.mediterranee.presentation.vo.emballages.TypeEmballageVO;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TypeEmballageVoFactory {

    /**
     * Convertir une liste de type emballage en une liste de VO de type emballages.
     *
     * @param typeEmballages les types emballages à convertir en VO.
     * @return la liste de VO des types emballages qui ont été transformé.
     */
    public List<TypeEmballageVO> typeEmballagesVO(List<TypeEmballage> typeEmballages) {
        return typeEmballages.stream()
                .map(TypeEmballageVO::new)
                .collect(toList());
    }

    /**
     * Convertir un type emballage en un VO.
     *
     * @param typeEmballage le type emballage.
     * @return le VO du type emballage.
     */
    public TypeEmballageVO typeEmballageVO(TypeEmballage typeEmballage) {
        return new TypeEmballageVO(typeEmballage);
    }
}
