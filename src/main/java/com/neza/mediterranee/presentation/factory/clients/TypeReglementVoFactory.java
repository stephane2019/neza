package com.neza.mediterranee.presentation.factory.clients;

import com.neza.mediterranee.domain.clients.TypeReglement;
import com.neza.mediterranee.presentation.vo.clients.TypeReglementVO;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TypeReglementVoFactory {

    /**
     * Convertir une liste de type reglement en une liste de VO de type reglement.
     *
     * @param typeReglements les types reglements à convertir en VO.
     * @return la liste de VO des types emballages qui ont été transformé.
     */
    public List<TypeReglementVO> typesReglementsVO(List<TypeReglement> typeReglements) {
        return typeReglements.stream()
                .map(TypeReglementVO::new)
                .collect(toList());
    }

    /**
     * Convertir un type reglement en un VO.
     *
     * @param typeReglement le type reglement.
     * @return le VO du type reglement.
     */
    public TypeReglementVO typeReglementVO(TypeReglement typeReglement) {
        return new TypeReglementVO(typeReglement);
    }
}

