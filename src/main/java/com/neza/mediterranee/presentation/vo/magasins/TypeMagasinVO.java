package com.neza.mediterranee.presentation.vo.magasins;

import com.neza.mediterranee.domain.magasins.TypeMagasin;
import com.sun.istack.NotNull;

/**
 * VO représentant un type magasin
 */
public class TypeMagasinVO {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String designation;

    /**
     * Constructeur par défaut.
     */
    public TypeMagasinVO() {
    }

    /**
     * Constructeur parametré.
     *
     * @param typeMagasin le type magasin.
     */
    public TypeMagasinVO(TypeMagasin typeMagasin) {
        this.id = typeMagasin.getId();
        this.code = typeMagasin.getCode();
        this.designation = typeMagasin.getDesignation();
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }
}

