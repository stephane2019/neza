package com.neza.mediterranee.presentation.vo.emballages;

import com.neza.mediterranee.domain.emballages.TypeEmballage;
import com.sun.istack.NotNull;

/**
 * VO représentant un TypeEmballage
 */
public class TypeEmballageVO {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String designation;

    /**
     * Constructeur par défaut.
     */
    public TypeEmballageVO() {
    }

    /**
     * Constructeur parametré.
     *
     * @param typeEmballage le type emballage.
     */
    public TypeEmballageVO(TypeEmballage typeEmballage) {
        this.id = typeEmballage.getId();
        this.code = typeEmballage.getCode();
        this.designation = typeEmballage.getDesignation();
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }
}
