package com.neza.mediterranee.presentation.vo.clients;

import com.neza.mediterranee.domain.clients.TypeReglement;
import com.sun.istack.NotNull;

/**
 * VO représentant un TypeReglement
 */
public class TypeReglementVO {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String designation;

    /**
     * Constructeur par défaut
     */
    public TypeReglementVO() {
    }

    /**
     * Constructeur parametré.
     *
     * @param typeReglement le type emballage.
     */
    public TypeReglementVO(TypeReglement typeReglement) {
        this.id = typeReglement.getId();
        this.code = typeReglement.getCode();
        this.designation = typeReglement.getDesignation();
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }
}

