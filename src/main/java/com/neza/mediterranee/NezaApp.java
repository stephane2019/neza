package com.neza.mediterranee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NezaApp {

    public static void main(String[] args) {
        SpringApplication.run(NezaApp.class, args);
    }

}
