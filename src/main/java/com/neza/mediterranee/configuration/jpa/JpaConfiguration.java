package com.neza.mediterranee.configuration.jpa;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@Configuration
@EntityScan(
		basePackages = {"com.neza.mediterranee"},
		basePackageClasses = {Jsr310JpaConverters.class}
)
public class JpaConfiguration {

}
