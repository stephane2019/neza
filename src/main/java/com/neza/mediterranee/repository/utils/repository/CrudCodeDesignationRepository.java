package com.neza.mediterranee.repository.utils.repository;

import static com.neza.mediterranee.utils.ArrayUtils.toArray;
import static com.neza.mediterranee.utils.CollectionUtils.isNullOrEmpty;
import static org.apache.commons.collections4.CollectionUtils.subtract;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.neza.mediterranee.domain.utils.AbstractCodeDesignationEntity;
import com.neza.mediterranee.domain.utils.AbstractCodeDesignationEntity_;
import com.neza.mediterranee.repository.utils.exception.EntiteNonConnueException;

public interface CrudCodeDesignationRepository<T extends AbstractCodeDesignationEntity> extends CrudRepository<T> {

    default Optional<T> rechercherParCode(String code, QueryOptions<T>... queryOptions) {
        return rechercherParCodeImpl(code, queryOptions != null && queryOptions.length > 0 ? Optional.of(queryOptions[0]) : Optional.empty());
    }

    default T rechercherParCodeObligatoire(String code, QueryOptions<T>... queryOptions) {
        QueryOptions<T> options;
        if (queryOptions == null || queryOptions.length == 0) {
            options = QueryOptions.builder();
        }
        else {
            options = queryOptions[0];
        }
        options.useObligatoire();
        // si  l'entité n'est pas trouvé, la méthode ci-dessous lance une exception, c'est pour cette raison que l'on peut faire un .get sans tester la présence de l'entité
        return rechercherParCodeImpl(code, Optional.of(options)).get();
    }

    default Optional<T> rechercherParCodeImpl(String code, Optional<QueryOptions<T>> optionsOpt) {
        try {
            CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
            CriteriaQuery<T> criteriaQuery = builder.createQuery(getClazz());
            Root<T> root = criteriaQuery.from(getClazz());

            optionsOpt.ifPresent(options -> options.applyFetchOperations(root));

            criteriaQuery.where(
                    builder.equal(root.get(AbstractCodeDesignationEntity_.code), code)
            );

            TypedQuery<T> typedQuery = this.getEntityManager().createQuery(criteriaQuery);

            optionsOpt.ifPresent(options -> options.applyHints(typedQuery));

            return Optional.ofNullable(typedQuery.getSingleResult());
        }
        catch (NoResultException ex) {
            if (optionsOpt.isPresent() && optionsOpt.get().isObligatoire()) {
                throw new EntiteNonConnueException("erreur.aucunResultatPourCode", code);
            }
            return Optional.empty();
        }
    }

    default Map<String, T> rechercherParCodes(Collection<String> codes, QueryOptions<T>... queryOptions) {
        if (isNullOrEmpty(codes)) {
            return Collections.emptyMap();
        }

        // On cast en Set pour éviter les doublons
        Set<String> codesDistinct = new HashSet<>(codes);

        Optional<QueryOptions<T>> optionsOpt = queryOptions != null && queryOptions.length > 0 ? Optional.of(queryOptions[0]) : Optional.empty();

        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getClazz());
        Root<T> root = query.from(getClazz());

        query.select(root).where(root.get(AbstractCodeDesignationEntity_.code).in(codesDistinct));

        TypedQuery<T> typedQuery = this.getEntityManager().createQuery(query);

        optionsOpt.ifPresent(options -> options.applyHints(typedQuery));

        Map<String, T> resultat = typedQuery.getResultStream()
                .collect(Collectors.toMap(AbstractCodeDesignationEntity::getCode, Function.identity()));

        if (optionsOpt.isPresent() && optionsOpt.get().isObligatoire() && resultat.size() != codesDistinct.size()) {
            throw new EntiteNonConnueException("erreur.codesNonTrouves", subtract(codesDistinct, resultat.keySet()));
        }

        return resultat;
    }

    default Stream<T> rechercherParCodeDesignation(String filterCodeDesignation, Integer limit) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(getClazz());
        Root<T> root = query.from(getClazz());

        query.select(root);

        List<Predicate> predicates = new ArrayList<>();

        if (filterCodeDesignation != null) {
            predicates.add(
                    builder.or(
                            PredicatesUtils.startsWith(builder, root.get(AbstractCodeDesignationEntity_.code), filterCodeDesignation.toUpperCase()),
                            PredicatesUtils.localeContains(builder, builder.upper(root.get(AbstractCodeDesignationEntity_.designation)), filterCodeDesignation.toUpperCase())
                    ));
        }
        return getResultats(builder, query, root, predicates, limit);
    }

    /**
     * Recherche les resultat selon le filtre sur le code/designation en prenant en compte les prédicats spécifiés.
     *
     * @param builder le builder.
     * @param query la query.
     * @param root le root Famille.
     * @param predicates les prédicats à prendre en compte.
     * @param limit la limite de recherche.
     * @return les resultats.
     */
    default Stream<T> getResultats(CriteriaBuilder builder, CriteriaQuery<T> query, Root<T> root, List<Predicate> predicates, Integer limit) {
        query.distinct(true);
        query.where(toArray(Predicate.class, predicates));
        query.orderBy(builder.asc(root.get(AbstractCodeDesignationEntity_.code)));

        TypedQuery<T> typedQuery = getEntityManager().createQuery(query);

        if (limit != null && limit > 0) {
            typedQuery.setMaxResults(limit);
        }

        return typedQuery.getResultStream();
    }
}
