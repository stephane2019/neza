package com.neza.mediterranee.repository.utils.repository;

import static java.util.stream.Collectors.toSet;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Collection;
import java.util.Set;

import com.neza.mediterranee.domain.utils.AbstractCodeDesignationEntity;
import com.neza.mediterranee.domain.utils.AbstractCodeDesignationEntity_;
import com.neza.mediterranee.domain.utils.AbstractEntity;
import com.neza.mediterranee.utils.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Classe utilitaire générant des predicates utilisés couramment.
 */
public class PredicatesUtils {

    /**
     * Wildcard pour les requêtes en LIKE.
     */
    protected static final String WILDCARD = "%";
    protected static final int NOMBRE_DECIMALES = 4;
    protected static final String SEPARATEUR_DECIMAL = ".";
    private static final String PG_TRANSLATE = "translate";
    private static final String PG_TRANSLATE_FROM = "'áàâãäéèêëíìïîóòôõöúùûüÁÀÂÃÄÉÈÊËÍÌÏÓÒÔÕÖÚÙÛÜçÇ'";
    private static final String PG_TRANSLATE_TO = "'aaaaaeeeeiiiiooooouuuuAAAAAEEEEIIIOOOOOUUUUcC'";

    private PredicatesUtils() {
        super();
        // Util class
    }

    /**
     * <p>Renvoie un prédicat permettant de filtrer sur le champ code ou le champ désignation d'une entité ayant un code et une désignation. </p>
     * <ul>
     * <li>la recherche sur le code se fait en "commence par".</li>
     * <li>la recherche sur la désignation se fait en "contient".</li>
     * </ul>
     *
     * @param builder le builder courant.
     * @param from le from de l'entité.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur le champ code ou le champ désignation d'une entité.
     */
    public static <T extends AbstractCodeDesignationEntity> Predicate codeStartsWithOrDesignationContains(CriteriaBuilder builder, From<?, T> from, String filtre) {
        return builder.or(
                startsWith(builder, from.get(AbstractCodeDesignationEntity_.code), filtre),
                localeContains(builder, from.get(AbstractCodeDesignationEntity_.designation), filtre)
        );
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ via une recherche de type "commence par".
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ via une recherche de type "commence par".
     */
    public static Predicate startsWith(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return builder.like(builder.lower(attribute), filtre.toLowerCase() + WILDCARD);
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ via une recherche de type "contient".
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ via une recherche de type "contient".
     */
    public static Predicate contains(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return builder.like(builder.lower(attribute), WILDCARD + filtre.toLowerCase() + WILDCARD);
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ via une recherche de type "commence par" en ne tenant pas
     * compte des accents.
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ via une recherche de type "commence par".
     */
    public static Predicate localeStartsWith(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return localeLike(builder, attribute, filtre.toLowerCase() + WILDCARD);
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ via une recherche de type "contient".
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ via une recherche de type "contient".
     */
    public static Predicate localeContains(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return localeLike(builder, attribute, WILDCARD + filtre.toLowerCase() + WILDCARD);
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ via une recherche de type "egale".
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ via une recherche de type "egale".
     */
    public static Predicate localeEquals(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return localeLike(builder, attribute, filtre);
    }

    /**
     * Renvoie un prédicat permettant de filtrer sur un champ decimal via une recherche de type "egale".
     *
     * @param builder le builder courant.
     * @param attribute le champ sur lequel filtrer.
     * @param filtre le filtre à appliquer.
     * @return un prédicat permettant de filtrer sur un champ decimal via une recherche de type "egale".
     */
    public static Predicate decimaleEquals(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        //formatage du decimal au format 0.0000
        return builder.equal(attribute, filtre.contains(SEPARATEUR_DECIMAL) ?
                filtre + StringUtils.repeat('0', NOMBRE_DECIMALES - StringUtils.reverse(filtre).indexOf(SEPARATEUR_DECIMAL)) :
                filtre + SEPARATEUR_DECIMAL + StringUtils.repeat('0', NOMBRE_DECIMALES));
    }

    private static Predicate localeLike(CriteriaBuilder builder, Expression<String> attribute, String filtre) {
        return builder.like(functionAccentInsensitivePostgres(builder, attribute), functionAccentInsensitivePostgres(builder, builder.literal(filtre)));
    }

    private static Expression<String> functionAccentInsensitivePostgres(CriteriaBuilder cb, Expression<String> exp) {
        return cb.function(
                PG_TRANSLATE,
                String.class,
                cb.lower(exp),
                cb.literal(PG_TRANSLATE_FROM),
                cb.literal(PG_TRANSLATE_TO));
    }

    /**
     * Renvoie le prédicat correspondant à un 'IN'.
     * http://stackoverflow.com/questions/28153756/hibernate-performance-issues
     *
     * @param builder le builder courant.
     * @param pathToEntity le chemin vers l'entité vers lequel on veut faire un IN
     * @param entities les entités parmi lesquelles pathToEntity doit être.
     * @param idAttribute l'attribut permettant d'accéder à l'id des entités de entities.
     * @param <T> le type d'entité.
     * @return le prédicat.
     */
    public static <T extends AbstractEntity> Predicate predicateIn(CriteriaBuilder builder, Path<T> pathToEntity, Collection<T> entities, SingularAttribute<T, Long> idAttribute) {
        // Perfs optimization
        // Use id instead of entity
        // See: http://stackoverflow.com/questions/28153756/hibernate-performance-issues
        Set<Long> entitiesIds = entities.stream()
                .map(AbstractEntity::getId)
                .collect(toSet());

        Path<Long> idPath = pathToEntity.get(idAttribute);
        return entitiesIds.size() == 1 ?
                builder.equal(idPath, CollectionUtils.first(entitiesIds)) :
                idPath.in(entitiesIds);
    }
}