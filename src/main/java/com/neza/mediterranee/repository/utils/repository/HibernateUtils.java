package com.neza.mediterranee.repository.utils.repository;

import com.neza.mediterranee.domain.utils.AbstractEntity;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;


public class HibernateUtils {

    /**
     * Déproxifie l'entité hibernate.
     *
     * @param entite l'entité à déproxifier.
     * @param <T> le type de l'entité.
     * @return l'entité déproxifiée.
     */
    public static <T extends AbstractEntity> T deproxifier(T entite) {
        Hibernate.initialize(entite);
        if (entite instanceof HibernateProxy) {
            //noinspection unchecked
            return (T) ((HibernateProxy) entite).getHibernateLazyInitializer().getImplementation();
        }
        return entite;
    }

}
