package com.neza.mediterranee.repository.utils.exception;

import java.text.MessageFormat;

/**
 * Classe parente pour toutes les exceptions liées à un code erreur.<br/>
 * Le code erreur doit implémenter l'interface HasCodeErreur.<br/>
 * Chaque code erreur doit être utilisé par une et une seule exception.
 *
 * @see HasCodeErreur
 */
@SuppressWarnings("serial")
public abstract class AbstractApplicationException extends RuntimeException {

    private String cleTraduction;

    private String messageTraduit;

    /**
     * Chaque exception étendant cette classe doit définir un code unique permettant de retrouver rapidement le type d'erreur applicative rencontrée. Le code doit être déclaré dans l'enum code erreur
     * afin d'assurer l'unicité du code.
     */
    private HasCodeErreur codeErreur;

    /**
     * Les paramètres éventuels pour créer le message de l'exception.
     */
    private Object[] params;

    /**
     * La façon dont l'exception est prévue pour être affichée à l'utilisateur.
     */
    protected ExceptionUserDisplay display;

    public AbstractApplicationException(HasCodeErreur codeErreur, String cleTraduction, Object... params) {
        super(cleTraduction);
        this.codeErreur = codeErreur;
        this.cleTraduction = cleTraduction;
        this.params = params;
        this.display = ExceptionUserDisplay.ERROR;
    }

    public AbstractApplicationException(HasCodeErreur codeErreur, ExceptionUserDisplay display, String cleTraduction, Object... params) {
        this(codeErreur, cleTraduction, params);
        this.display = display;
    }

    public HasCodeErreur getCodeErreur() {
        return codeErreur;
    }


    /**
     * Retourne un message préformaté avec le code erreur et le message.
     *
     * @return le message d'erreur.
     */
    public final String getMessageAvecCode() {
        return MessageFormat.format("[{0,number,0000}] {1}", codeErreur.getCode(), getMessage());
    }

    public String getCleTraduction() {
        return cleTraduction;
    }

    public ExceptionUserDisplay getDisplay() {
        return display;
    }

    public Object[] getParams() {
        return params;
    }

    private String getMessageTraduit() {
        return messageTraduit;
    }

    public void setMessageTraduit(String messageTraduit) {
        this.messageTraduit = messageTraduit;
    }

    @Override
    public String getMessage() {
        return getMessageTraduit() == null ? super.getMessage() : getMessageTraduit();
    }

}
