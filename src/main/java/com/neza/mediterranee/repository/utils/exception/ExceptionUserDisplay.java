package com.neza.mediterranee.repository.utils.exception;

/**
 * Le type d'affichage prévu pour les exceptions.
 */
public enum ExceptionUserDisplay {
    ERROR, WARNING, INFO;
}
