package com.neza.mediterranee.repository.emballages;

import com.neza.mediterranee.domain.emballages.Emballage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface EmballageRepository extends JpaRepository<Emballage, Integer>, JpaSpecificationExecutor<Emballage> {
}
