package com.neza.mediterranee.repository.emballages;

import com.neza.mediterranee.domain.emballages.TypeEmballage;
import com.neza.mediterranee.domain.emballages.TypeEmballage_;
import com.neza.mediterranee.repository.utils.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.neza.mediterranee.utils.ArrayUtils.toArray;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Repository
public class TypeEmballageRepository implements CrudRepository<TypeEmballage> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<TypeEmballage> getClazz() {
        return TypeEmballage.class;
    }

    /**
     * Recherhe le type emballage à partir de son code.
     *
     * @param code le code du type emballage.
     * @return le type emballage recherché.
     */
    public TypeEmballage rechercherParCode(String code) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeEmballage> criteriaQuery = criteriaBuilder.createQuery(TypeEmballage.class);
        Root<TypeEmballage> root = criteriaQuery.from(TypeEmballage.class);

        criteriaQuery.select(root)
                .where(
                        criteriaBuilder.equal(root.get(TypeEmballage_.code), code)
                );

        try {
            return entityManager.createQuery(criteriaQuery).getSingleResult();
        }
        catch (Exception ex) {
            return null;
        }
    }

    /**
     * Récupère l'ensemble des types emballages en fonction des critères.
     *
     * @param code le code du type emballage.
     * @param designation la designation du type emballage.
     * @return les types emballages respectant les critères de recherche.
     */
    public List<TypeEmballage> listerTypesEmballage(String code, String designation) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeEmballage> query = builder.createQuery(TypeEmballage.class);
        Root<TypeEmballage> root = query.from(TypeEmballage.class);

        List<Predicate> predicates = new ArrayList<>();

        if (isNotBlank(code)) {
            predicates.add(builder.equal(root.get(TypeEmballage_.code), code));
        }

        if (designation != null) {
            predicates.add(builder.equal(root.get(TypeEmballage_.designation), designation));
        }

        query.orderBy(builder.asc(root.get(TypeEmballage_.code)));
        query.where(toArray(Predicate.class, predicates));

        return entityManager.createQuery(query).getResultList();
    }
}
