package com.neza.mediterranee.repository.magasins;

import com.neza.mediterranee.domain.magasins.TypeMagasin;
import com.neza.mediterranee.domain.magasins.TypeMagasin_;
import com.neza.mediterranee.repository.utils.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.neza.mediterranee.utils.ArrayUtils.toArray;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Repository
public class TypeMagasinRepository implements CrudRepository<TypeMagasin> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<TypeMagasin> getClazz() {
        return TypeMagasin.class;
    }

    /**
     * Recherhe le type magasin à partir de son code.
     *
     * @param code le code du type emballage.
     * @return le type magasin recherché.
     */
    public TypeMagasin rechercherParCode(String code) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeMagasin> criteriaQuery = criteriaBuilder.createQuery(TypeMagasin.class);
        Root<TypeMagasin> root = criteriaQuery.from(TypeMagasin.class);

        criteriaQuery.where(
                        criteriaBuilder.equal(root.get(TypeMagasin_.code), code)
                );

        try {
            return entityManager.createQuery(criteriaQuery).getSingleResult();
        }
        catch (Exception ex) {
            return null;
        }
    }

    /**
     * Récupère l'ensemble des types magasins en fonction des critères.
     *
     * @param code le code du type magasin.
     * @param designation la designation du type magasin.
     * @return les types magasins respectant les critères de recherche.
     */
    public List<TypeMagasin> listerTypesMagasins(String code, String designation) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeMagasin> query = builder.createQuery(TypeMagasin.class);
        Root<TypeMagasin> root = query.from(TypeMagasin.class);

        List<Predicate> predicates = new ArrayList<>();

        if (isNotBlank(code)) {
            predicates.add(builder.equal(root.get(TypeMagasin_.code), code));
        }

        if (designation != null) {
            predicates.add(builder.equal(root.get(TypeMagasin_.designation), designation));
        }

        query.orderBy(builder.asc(root.get(TypeMagasin_.code)));
        query.where(toArray(Predicate.class, predicates));

        return entityManager.createQuery(query).getResultList();
    }
}
