package com.neza.mediterranee.repository.clients;

import com.neza.mediterranee.domain.clients.TypeReglement;
import com.neza.mediterranee.domain.clients.TypeReglement_;
import com.neza.mediterranee.repository.utils.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.neza.mediterranee.utils.ArrayUtils.toArray;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Repository
public class TypeReglementRepository implements CrudRepository<TypeReglement> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Class<TypeReglement> getClazz() {
        return TypeReglement.class;
    }

    /**
     * Recherhe le type reglement à partir de son code.
     *
     * @param code le code du type reglement.
     * @return le type reglement recherché.
     */
    public TypeReglement rechercherParCode(String code) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeReglement> criteriaQuery = criteriaBuilder.createQuery(TypeReglement.class);
        Root<TypeReglement> root = criteriaQuery.from(TypeReglement.class);

        criteriaQuery.where(
                        criteriaBuilder.equal(root.get(TypeReglement_.code), code)
                );

        try {
            return entityManager.createQuery(criteriaQuery).getSingleResult();
        }
        catch (Exception ex) {
            return null;
        }
    }

    /**
     * Récupère l'ensemble des types reglements en fonction des critères.
     *
     * @param code le code du type reglement.
     * @param designation la designation du type reglement.
     * @return les types reglements respectant les critères de recherche.
     */
    public List<TypeReglement> listerTypesReglements(String code, String designation) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeReglement> query = builder.createQuery(TypeReglement.class);
        Root<TypeReglement> root = query.from(TypeReglement.class);

        List<Predicate> predicates = new ArrayList<>();

        if (isNotBlank(code)) {
            predicates.add(builder.equal(root.get(TypeReglement_.code), code));
        }

        if (designation != null) {
            predicates.add(builder.equal(root.get(TypeReglement_.designation), designation));
        }

        query.orderBy(builder.asc(root.get(TypeReglement_.code)));
        query.where(toArray(Predicate.class, predicates));

        return entityManager.createQuery(query).getResultList();
    }
}
