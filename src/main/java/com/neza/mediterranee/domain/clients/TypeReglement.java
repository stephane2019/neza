package com.neza.mediterranee.domain.clients;

import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = TypeReglement.TABLE_NAME)
public class TypeReglement extends AbstractEntity {

    public static final String TABLE_NAME = "type_reglement";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    /**
     * Constructeur par defaut
     */
    public TypeReglement() {
    }

    /**
     * Constructeur parametré.
     *
     * @param code le code du type reglement.
     * @param designation la designation du type reglement.
     */
    public TypeReglement(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    /**
     * Permet de renseigner un type reglement.
     *
     * @param code le code du type reglement.
     * @param designation la designation du type reglement.
     */
    public void renseignerTypeReglement(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    @Override
    public Long getId() {
        return id;
    }
}
