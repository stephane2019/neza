package com.neza.mediterranee.domain.clients;


import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = Reglement.TABLE_NAME)
public class Reglement extends AbstractEntity {
    public static final String TABLE_NAME = "reglement";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = OperationClient.TABLE_ID)
    private OperationClient operationClient;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = TypeReglement.TABLE_ID)
    private TypeReglement typeReglement;

    @Column(name = "montant_reglement", nullable = false)
    private Long montantReglement;

    /**
     * Constructeur par defaut
     */
    public Reglement() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public OperationClient getOperationClient() {
        return operationClient;
    }

    public TypeReglement getTypeReglement() {
        return typeReglement;
    }

    public Long getMontantReglement() {
        return montantReglement;
    }

    @Override
    public Long getId() {
        return id;
    }
}