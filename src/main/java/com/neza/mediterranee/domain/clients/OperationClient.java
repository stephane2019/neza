package com.neza.mediterranee.domain.clients;

import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = OperationClient.TABLE_NAME)
public class OperationClient extends AbstractEntity {
    public static final String TABLE_NAME = "operation_client";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = Client.TABLE_ID, referencedColumnName = "id")
    private Client client;

    /**
     * Constructeur par defaut
     */
    public OperationClient() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public Client getClient() {
        return client;
    }

    @Override
    public Long getId() {
        return id;
    }
}
