package com.neza.mediterranee.domain.clients;

import com.neza.mediterranee.domain.produits.Produit;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = OperationLigneClient.TABLE_NAME)
public class OperationLigneClient extends AbstractEntity {
    public static final String TABLE_NAME = "ligne_operation_client";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = OperationClient.TABLE_ID, referencedColumnName = "id")
    private List<OperationClient> operationClients = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = Produit.TABLE_ID, referencedColumnName = "id")
    private List<Produit> produits = new ArrayList<>();

    /**
     * Constructeur par defaut
     */
    public OperationLigneClient() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public List<OperationClient> getOperationClients() {
        return operationClients;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    @Override
    public Long getId() {
        return id;
    }
}
