package com.neza.mediterranee.domain.clients;


import com.neza.mediterranee.domain.utilisateurs.Utilisateur;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = Client.TABLE_NAME)
public class Client extends AbstractEntity {

    public static final String TABLE_NAME = "client";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "nom", nullable = false)
    private String nomClient;

    @Column(name = "contacts", nullable = false)
    private String contacts;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "client_proprietaire_id")
    private Long proprietaireClient;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "compte_client_id", referencedColumnName = "id")
    private Utilisateur compteClient;

    /**
     * Constructeur par défaut.
     */
    public Client() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public String getNomClient() {
        return nomClient;
    }

    public String getContacts() {
        return contacts;
    }

    public String getAdresse() {
        return adresse;
    }

    public Long getProprietaireClient() {
        return proprietaireClient;
    }

    public Utilisateur getCompteClient() {
        return compteClient;
    }
}
