package com.neza.mediterranee.domain.emballages;


import com.neza.mediterranee.domain.fournisseurs.Fournisseur;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = Emballage.TABLE_NAME)
public class Emballage extends AbstractEntity {

    public static final String TABLE_NAME = "emballage";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = TypeEmballage.TABLE_ID, nullable = false, referencedColumnName = "id")
    private TypeEmballage typeEmballage;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = Fournisseur.TABLE_ID, referencedColumnName = "id")
    private Fournisseur fournisseur;

    /**
     * Constructeur par défaut.
     */
    public Emballage() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public TypeEmballage getTypeEmballage() {
        return typeEmballage;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    @Override
    public Long getId() {
        return id;
    }
}
