package com.neza.mediterranee.domain.emballages;


import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = TypeEmballage.TABLE_NAME)
public class TypeEmballage extends AbstractEntity {

    public static final String TABLE_NAME = "type_emballage";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    /**
     * Constructeur par défaut.
     */
    public TypeEmballage() {
    }

    /**
     * Constructeur parametré.
     *
     * @param code le code du type emballage.
     * @param designation la designation du type emballage.
     */
    public TypeEmballage(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    /**
     * Permet de renseigner un type emballage.
     *
     * @param code le code du type emballage.
     * @param designation la designation du type emballage.
     */
    public void renseignerTypeEmballage(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    @Override
    public Long getId() {
        return id;
    }
}

