package com.neza.mediterranee.domain.produits;

import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "type_produit")
public class TypeProduit extends AbstractEntity {

    public static final String TABLE_NAME = "type_magasin";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @Override
    public Long getId() {
        return id;
    }
}
