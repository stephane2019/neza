package com.neza.mediterranee.domain.produits;

import com.neza.mediterranee.domain.emballages.Emballage;
import com.neza.mediterranee.domain.fournisseurs.Fournisseur;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = Produit.TABLE_NAME)
public class Produit extends AbstractEntity {

    public static final String TABLE_NAME = "produit";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "date_peremption")
    private String datePeremption;

    @Column(name = "adresse", nullable = false)
    private String adresse;

    @Column(name = "volume", nullable = false)
    private String volume;

    @Column(name = "poids", nullable = false)
    private String poids;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = Emballage.TABLE_ID)
    private Emballage emballage;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = TypeProduit.TABLE_ID)
    private TypeProduit typeProduit;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = Fournisseur.TABLE_ID)
    private Fournisseur fournisseur;

    /**
     * Constructeur par defaut
     */
    public Produit() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public String getDatePeremption() {
        return datePeremption;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getVolume() {
        return volume;
    }

    public String getPoids() {
        return poids;
    }

    public Emballage getEmballage() {
        return emballage;
    }

    public TypeProduit getTypeProduit() {
        return typeProduit;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }
}
