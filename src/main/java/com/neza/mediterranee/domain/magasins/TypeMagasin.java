package com.neza.mediterranee.domain.magasins;


import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = TypeMagasin.TABLE_NAME)
public class TypeMagasin extends AbstractEntity {

    public static final String TABLE_NAME = "type_magasin";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    /**
     * Constructeur par défaut.
     */
    public TypeMagasin() {
    }

    /**
     * Constructeur parametré.
     *
     * @param code le code du type magasin.
     * @param designation la designation du type magasin.
     */
    public TypeMagasin(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    /**
     * Permet de renseigner un type magasin.
     *
     * @param code le code du type magasin.
     * @param designation la designation du type magasin.
     */
    public void renseignerTypeMagasin(String code, String designation) {
        this.code = code;
        this.designation = designation;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }
}
