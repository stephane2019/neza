package com.neza.mediterranee.domain.magasins;


import com.neza.mediterranee.domain.fournisseurs.Fournisseur;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = OperationMagasin.TABLE_NAME)
public class OperationMagasin extends AbstractEntity {
    public static final String TABLE_NAME = "operation_magasin";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = Magasin.TABLE_ID)
    private List<Magasin> magasins = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = Fournisseur.TABLE_ID)
    private List<Fournisseur> fournisseurs = new ArrayList<>();

    /**
     * Constructeur par defaut
     */
    public OperationMagasin() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public List<Magasin> getMagasins() {
        return magasins;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    @Override
    public Long getId() {
        return id;
    }
}
