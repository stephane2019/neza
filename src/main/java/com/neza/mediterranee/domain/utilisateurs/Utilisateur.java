package com.neza.mediterranee.domain.utilisateurs;

import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = Utilisateur.TABLE_NAME)
public class Utilisateur extends AbstractEntity {

    public static final String TABLE_NAME = "utilisateur";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = Profil.TABLE_ID)
    private List<Profil> profils = new ArrayList<>();

    /**
     * Constructeur par défaut.
     */
    public Utilisateur() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public List<Profil> getProfils() {
        return profils;
    }
}
