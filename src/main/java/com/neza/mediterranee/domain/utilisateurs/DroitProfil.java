package com.neza.mediterranee.domain.utilisateurs;


import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = DroitProfil.TABLE_NAME)
public class DroitProfil extends AbstractEntity {

    public static final String TABLE_NAME = "droits_profil";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = Droit.TABLE_ID, nullable = false)
    private List<Droit> droits = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = Profil.TABLE_ID, nullable = false)
    private List<Profil> profils = new ArrayList<>();

    /**
     * Constructeur par defaut
     */
    public DroitProfil() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public List<Droit> getDroits() {
        return droits;
    }

    public List<Profil> getProfils() {
        return profils;
    }
}


