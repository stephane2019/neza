package com.neza.mediterranee.domain.utilisateurs;

import org.dom4j.tree.AbstractEntity;

import javax.persistence.*;

import static com.neza.mediterranee.domain.utils.JpaConstants.ID;
import static com.neza.mediterranee.domain.utils.JpaConstants.SEQ;

@Entity
@Table(name = Profil.TABLE_NAME)
public class Profil extends AbstractEntity {

    public static final String TABLE_NAME = "profil";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    /**
     * Constructeur par defaut
     */
    public Profil() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }
}
