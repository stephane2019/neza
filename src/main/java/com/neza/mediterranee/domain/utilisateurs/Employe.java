package com.neza.mediterranee.domain.utilisateurs;


import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = Employe.TABLE_NAME)
public class Employe extends AbstractEntity {

    public static final String TABLE_NAME = "employe";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "type", nullable = false)
    private String typeEmploye;

    @Column(name = "matricule", nullable = false, unique = true)
    private String matricule;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom", nullable = false)
    private String prenom;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "contact1", nullable = false)
    private String contact1;

    @Column(name = "contact2", nullable = false)
    private String contact2;

    @Column(name = "date_embauche", nullable = false)
    private LocalDateTime dateEmbauche;

    @Column(name = "date_fin_contrat", nullable = false)
    private LocalDateTime dateFinContrat;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "compte_employe_id")
    private Utilisateur compteEmploye;

    /**
     * Constructeur par defaut
     */
    public Employe() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getTypeEmploye() {
        return typeEmploye;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getContact1() {
        return contact1;
    }

    public String getContact2() {
        return contact2;
    }

    public LocalDateTime getDateEmbauche() {
        return dateEmbauche;
    }

    public LocalDateTime getDateFinContrat() {
        return dateFinContrat;
    }

    public Utilisateur getCompteEmploye() {
        return compteEmploye;
    }
}
