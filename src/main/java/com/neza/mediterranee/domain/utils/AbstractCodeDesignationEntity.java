package com.neza.mediterranee.domain.utils;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

/**
 * Classe parente pour une donnée du référentiel.<br/>
 * Elle définit un code, qui doit être unique, et une désignation
 */
@MappedSuperclass
public abstract class AbstractCodeDesignationEntity extends AbstractEntity {

    /**
     * Code
     */
    @Column(name = "code", nullable = false, unique = true, updatable = false)
    protected String code;

    /**
     * Désignation
     */
    @Column(name = "designation", nullable = false)
    protected String designation;

    public AbstractCodeDesignationEntity() {
        super();
    }

    protected AbstractCodeDesignationEntity(String code, String designation) {
        this();
        this.code = code;
        this.designation = designation;
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    protected boolean equalsOnOtherFields(AbstractEntity other) {
        return Objects.equals(code, ((AbstractCodeDesignationEntity) other).code);
    }

    @Override
    public String toString() {
        return String.format("%s{id=%s, code=%s, designation=%s}", getClass().getSimpleName(), getId(), getCode(), getDesignation());
    }
}
