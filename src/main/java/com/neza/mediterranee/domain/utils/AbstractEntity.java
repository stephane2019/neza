package com.neza.mediterranee.domain.utils;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

/**
 * Classe parente de tous les objets du domaine managés par JPA.<br/>
 * Elle implémente notamment un comportement par défaut sur les méthodes equals() et hashcode() pertinent pour des objets JPA.
 *
 * @see @Entity
 */
@MappedSuperclass
public abstract class AbstractEntity implements JpaConstants {

    @Version
    protected long version;

    /**
     * Contient les informations concernant la création de l'objet :
     * <ul>
     * <li>le login de l'utilisateur ayant crée la donnée</li>
     * <li>la date de création  de la donnée</li>
     * </ul>
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "date", column = @Column(name = "audit_date_creation", updatable = false)),
            @AttributeOverride(name = "login", column = @Column(name = "audit_login_creation", updatable = false))}
    )
    private DateEtLogin creation;

    /**
     * Contient les informations concernant la modification de l'objet :
     * <ul>
     * <li>le login de l'utilisateur ayant modifié la donnée</li>
     * <li>la date de modification  de la donnée</li>
     * </ul>
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "date", column = @Column(name = "audit_date_modification")),
            @AttributeOverride(name = "login", column = @Column(name = "audit_login_modification"))}
    )
    private DateEtLogin modification;

    public AbstractEntity() {
        super();
    }

    @Transient
    public boolean isNew() {
        return getId() == null;
    }

    /**
     * Id de l'entité.
     *
     * @return Id de l'entité.
     */
    public abstract Long getId();

    public long getVersion() {
        return version;
    }

    public DateEtLogin getCreation() {
        return creation;
    }

    public DateEtLogin getModification() {
        return modification;
    }

    /**
     * Récupère la date de création de l'objet.
     *
     * @return la date de création.
     */
    public Instant dateCreation() {
        return creation != null ? creation.getDate() : null;
    }

    /**
     * Récupère le login de création de l'objet.
     *
     * @return le login de création.
     */
    public String loginCreation() {
        return creation != null ? creation.getLogin() : null;
    }

    /**
     * Récupère la date de modification de l'objet.
     *
     * @return la date de modification.
     */
    public Instant dateModification() {
        return modification != null ? modification.getDate() : null;
    }

    /**
     * Récupère le login de modification de l'objet.
     *
     * @return le login de modification.
     */
    public String loginModification() {
        return modification != null ? modification.getLogin() : null;
    }

    /**
     * Initialise les informations de tracabilité sur la donnée.<br/>
     * Valorise les informations concernant le login de la date de création.<br/>
     * Valorise les informations concernant le login de la date de modification.
     */
    @PrePersist
    private void initialiserInformationsTracabilite() {
        String login = SecurityUtils.lireLoginUtilisateurConnecte();
        Instant now = Instant.now();
        if (creation == null) {
            creation = new DateEtLogin(login, now);
        }
        if (modification == null) {
            modification = new DateEtLogin(login, now);
        }
    }

    /**
     * Met à jour les informations concernant l'utilisateur et la date de modification.
     */
    @PreUpdate
    private void mettreAJourInformationsTracabilite() {
        if (modification == null) {
            modification = new DateEtLogin();
        }
        modification.update(Instant.now(), SecurityUtils.lireLoginUtilisateurConnecte());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (this.getClass().isAssignableFrom(o.getClass())) {
            AbstractEntity e = (AbstractEntity) o;
            // On se base sur les champs @Id si ils sont non null
            if (getId() != null && e.getId() != null) {
                return Objects.equals(getId(), e.getId());
            }

            return equalsOnOtherFields(e);
        }

        return false;
    }

    /**
     * Méthode appelée lorsque l'égalité sur l'id est fausse.<br/>
     * Permet de tester éventuellement d'autres champs pour l'égalité.<br/>
     * Par défaut, vérifie simplement l'égalité au sens de Object.<br/>
     * On garantit que other est non null en entrant dans cette méthode.
     *
     * @param other
     * @return
     */
    protected boolean equalsOnOtherFields(AbstractEntity other) {
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        // On se base sur le champ @Id si il est non null, sinon implémentation par défaut
        return getId() != null ? getId().hashCode() : super.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s{id=%s}", getClass().getSimpleName(), getId());
    }
}