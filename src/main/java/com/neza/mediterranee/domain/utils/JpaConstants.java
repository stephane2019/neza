package com.neza.mediterranee.domain.utils;


/**
 * Interfaces définissant des constantes pour tous les objets liés à JPA.
 */
public interface JpaConstants {

    /**
     * Suffixe utilisé dans les colonnes de jointures
     */
    String ID = "_ID";

    /**
     * Suffixe d'une séquence
     */
    String SEQ = "_SEQ";

    /**
     * Suffixe d'une Foreign Key
     */
    String FK = "_FK";
}