package com.neza.mediterranee.domain.fournisseurs;


import com.neza.mediterranee.domain.utilisateurs.Utilisateur;
import com.neza.mediterranee.domain.utils.AbstractEntity;

import javax.persistence.*;

@Entity
@Access(AccessType.FIELD)
@Table(name = Fournisseur.TABLE_NAME)
public class Fournisseur extends AbstractEntity {

    public static final String TABLE_NAME = "fournisseur";
    public static final String TABLE_ID = TABLE_NAME + ID;
    private static final String TABLE_SEQ = TABLE_ID + SEQ;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_SEQ)
    @SequenceGenerator(name = TABLE_SEQ, sequenceName = TABLE_SEQ)
    private Long id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "designation", nullable = false)
    private String designation;

    @Column(name = "contacts", nullable = false)
    private String contacts;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "fournisseur_proprietaire_id")
    private Long fournisseurProprietaire;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "compte_fournisseur_id")
    private Utilisateur compteFournisseur;

    /**
     * Constructeur par défaut.
     */
    public Fournisseur() {
    }

    public String getCode() {
        return code;
    }

    public String getDesignation() {
        return designation;
    }

    public String getContacts() {
        return contacts;
    }

    public String getAdresse() {
        return adresse;
    }

    public Long getFournisseurProprietaire() {
        return fournisseurProprietaire;
    }

    public Utilisateur getCompteFournisseur() {
        return compteFournisseur;
    }

    @Override
    public Long getId() {
        return id;
    }
}


