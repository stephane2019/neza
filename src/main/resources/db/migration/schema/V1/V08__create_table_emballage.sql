CREATE TABLE emballage (
  id                       INT8 CONSTRAINT emballage_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  fournisseur_id           INT8,
  type_emballage_id        INT8,

  CONSTRAINT emballage_code_uk UNIQUE(code),
  CONSTRAINT emballage_fournisseur_fk FOREIGN KEY (fournisseur_id) REFERENCES fournisseur(id),
  CONSTRAINT emballage_type_emballage_fk FOREIGN KEY (type_emballage_id) REFERENCES type_emballage(id)
);

CREATE SEQUENCE emballage_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;