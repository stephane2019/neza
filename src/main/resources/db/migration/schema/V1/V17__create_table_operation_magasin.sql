CREATE TABLE operation_magasin (
  id                       INT8 CONSTRAINT operation_magasin_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  magasin_id     INT8,
  fournisseur_id     INT8,

  CONSTRAINT operation_magasin_code_uk UNIQUE(code),
  CONSTRAINT operation_magasin_magasin_fk FOREIGN KEY (magasin_id) REFERENCES magasin(id),
  CONSTRAINT operation_magasin_fournisseur_fk FOREIGN KEY (fournisseur_id) REFERENCES fournisseur(id)
);

CREATE SEQUENCE operation_magasin_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;