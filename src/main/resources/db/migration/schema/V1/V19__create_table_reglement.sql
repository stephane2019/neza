CREATE TABLE reglement (
  id                       INT8 CONSTRAINT reglement_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  operation_client_id      INT8,
  type_reglement_id        INT8,
  montant_reglement        INT8,

  CONSTRAINT reglement_code_uk UNIQUE(code),
  CONSTRAINT reglement_operation_client_fk FOREIGN KEY (operation_client_id) REFERENCES operation_client(id),
  CONSTRAINT reglement_type_reglement_fk FOREIGN KEY (type_reglement_id) REFERENCES type_reglement(id)
);

CREATE SEQUENCE reglement_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;