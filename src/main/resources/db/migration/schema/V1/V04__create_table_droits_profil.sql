CREATE TABLE droits_profil (
  id                       INT8 CONSTRAINT droits_profil_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  droit_id         INT8,
  profil_id        INT8,

  CONSTRAINT droits_profil_uk UNIQUE (droit_id, profil_id),
  CONSTRAINT droits_profil_profil_fk FOREIGN KEY (profil_id) REFERENCES profil(id),
  CONSTRAINT droits_profil_droit_fk FOREIGN KEY (droit_id)  REFERENCES droit(id)
);

CREATE SEQUENCE droits_profil_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;