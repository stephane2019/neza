CREATE TABLE droit (
  id                       INT8 CONSTRAINT droit_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  CONSTRAINT droit_code_uk UNIQUE(code)
);

CREATE SEQUENCE droit_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;