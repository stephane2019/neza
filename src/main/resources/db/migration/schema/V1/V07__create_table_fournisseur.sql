CREATE TABLE fournisseur (
  id                       INT8 CONSTRAINT fournisseur_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  contacts                 VARCHAR(255),
  adresse                  VARCHAR(255),
  forunisseur_proprietaire_id   INT8,
  compte_fournisseur_id        INT8,

  CONSTRAINT fournisseur_fournisseur_proprietaire_fk FOREIGN KEY (forunisseur_proprietaire_id) REFERENCES fournisseur(id),
  CONSTRAINT fournisseur_compte_fournisseur_fk FOREIGN KEY (compte_fournisseur_id) REFERENCES utilisateur(id),

  CONSTRAINT fournisseur_code_uk UNIQUE(code)
);

CREATE SEQUENCE fournisseur_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;