CREATE TABLE produit (
  id                       INT8 CONSTRAINT produit_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  date_peremption      TIMESTAMP,
  adresse              VARCHAR(255)                    NOT NULL,
  volume               INT8,
  poids                INT8,
  emballage_id         INT8,
  fournisseur_id       INT8,
  type_produit_id      INT8,

  CONSTRAINT produit_type_produit_fk FOREIGN KEY (type_produit_id) REFERENCES type_produit(id),
  CONSTRAINT produit_emballage_fk FOREIGN KEY (emballage_id) REFERENCES emballage(id),
  CONSTRAINT produit_fournisseur_fk FOREIGN KEY (fournisseur_id) REFERENCES fournisseur(id),
  CONSTRAINT produit_code_uk UNIQUE(code)
);

CREATE SEQUENCE produit_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;