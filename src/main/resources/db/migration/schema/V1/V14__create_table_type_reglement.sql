CREATE TABLE type_reglement (
  id                       INT8 CONSTRAINT type_reglement_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,

  CONSTRAINT type_reglement_code_uk UNIQUE(code)
);

CREATE SEQUENCE type_reglement_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;