CREATE TABLE operation_client (
  id                       INT8 CONSTRAINT operation_client_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  client_id                 INT8,

  CONSTRAINT operation_client_code_uk UNIQUE(code),
  CONSTRAINT operation_client_client_fk FOREIGN KEY (client_id) REFERENCES client(id)
);

CREATE SEQUENCE operation_client_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;