CREATE TABLE employe (
  id                       INT8 CONSTRAINT employe_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  type                     varchar(255),
  matricule                VARCHAR(255)                NOT NULL,
  nom                      VARCHAR(255)                NOT NULL,
  prenom                   VARCHAR(255)                NOT NULL,
  email                     VARCHAR(255),
  contact1                 VARCHAR(255),
  contact2                 VARCHAR(255),
  date_embauche            TIMESTAMP,
  date_fin_contrat         TIMESTAMP,
  compte_employe_id        INT8,

  CONSTRAINT employe_matricule_uk UNIQUE(matricule),
  CONSTRAINT employe_compte_employe_fk FOREIGN KEY (compte_employe_id) REFERENCES utilisateur(id)
);

CREATE SEQUENCE employe_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;