CREATE TABLE ligne_operation_client (
  id                       INT8 CONSTRAINT ligne_operation_client_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  operation_client_id      INT8,
  produit_id               INT8,
  quantite                 INT8,

  CONSTRAINT ligne_operation_client_code_uk UNIQUE(code),
  CONSTRAINT ligne_operation_client_operation_client_fk FOREIGN KEY (operation_client_id) REFERENCES operation_client(id),
  CONSTRAINT ligne_operation_client_produit_fk FOREIGN KEY (produit_id) REFERENCES produit(id)
);

CREATE SEQUENCE ligne_operation_client_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;