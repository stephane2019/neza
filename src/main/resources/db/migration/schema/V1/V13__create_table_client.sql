CREATE TABLE client (
  id                       INT8 CONSTRAINT client_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  code                     VARCHAR(255)                NOT NULL,
  designation              VARCHAR(255)                NOT NULL,
  contacts                 VARCHAR(255),
  adresse                  VARCHAR(255),
  client_proprietaire_id   INT8,
  compte_client_id        INT8,

  CONSTRAINT client_client_proprietaire_fk FOREIGN KEY (client_proprietaire_id) REFERENCES client(id),
  CONSTRAINT client_compte_client_fk FOREIGN KEY (compte_client_id) REFERENCES utilisateur(id),

  CONSTRAINT client_code_uk UNIQUE(code)
);

CREATE SEQUENCE client_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;