CREATE TABLE utilisateur (
  id                       INT8 CONSTRAINT utilisateur_pk PRIMARY KEY,
  audit_date_creation      TIMESTAMP,
  audit_login_creation     VARCHAR(255),
  audit_date_modification  TIMESTAMP,
  audit_login_modification VARCHAR(255),
  version                  INT8 NOT NULL DEFAULT 0,
  login                   VARCHAR(255)                NOT NULL,
  password                 VARCHAR(255)                NOT NULL,
  profil_id                INT8,

  CONSTRAINT login_password_uk UNIQUE (login, password),
  CONSTRAINT utilisateur_profil_fk FOREIGN KEY (profil_id) REFERENCES profil(id),

  CONSTRAINT utilisateur_login_uk UNIQUE(login)
);

CREATE SEQUENCE utilisateur_id_seq MINVALUE 0 START WITH 50 INCREMENT BY 50;