import {NgModule} from '@angular/core';
import {ParametrageRoutingModule} from './parametrage-routing.module';
import {ParametrageTabsComponent} from './parametrage-tabs/parametrage-tabs.component';
import {CommonModule} from "@angular/common";
import {ListeClientComponent} from './liste-client/liste-client.component';

/**
 * Module de gestion des organisations
 */
@NgModule({
  imports: [
    ParametrageRoutingModule
  ],
  declarations: [
    ParametrageTabsComponent,
    ListeClientComponent
  ],
  exports: [
    CommonModule
  ]
})
export class ParametrageModule {
}
