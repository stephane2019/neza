import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrageTabsComponent } from './parametrage-tabs.component';

describe('ParametrageTabsComponent', () => {
  let component: ParametrageTabsComponent;
  let fixture: ComponentFixture<ParametrageTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParametrageTabsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrageTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
