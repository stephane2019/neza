import {RouterModule, Routes} from "@angular/router";
import {ParametrageTabsComponent} from "./parametrage-tabs/parametrage-tabs.component";
import {ListeClientComponent} from "./liste-client/liste-client.component";
import {NgModule} from "@angular/core";

/**
 * Routing interne au module des organisations
 */
const routes: Routes = [
  {
    path: '',
    component: ParametrageTabsComponent,
    children: [
      {
        path: '',
        redirectTo: 'client',
        pathMatch: 'full'
      },
      {
        path: 'app-liste-client',
        component: ListeClientComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametrageRoutingModule {
}
