import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccueilComponent} from "./accueil/accueil.component";
import {BrowserModule} from "@angular/platform-browser";
import {LoginComponent} from "./container/views/login/login.component";
import {DefaultLayoutComponent} from "./container";

const appRoutes: Routes = [
  {
    path: '', component: DefaultLayoutComponent,
    children: [
      {
        path: 'home', component: AccueilComponent
      },
      // {
      //   path: 'parametrage',
      //   loadChildren: () => import('parametrage/parametrage.module').then(m => m.ParametrageModule)
      // },
      {path: '', redirectTo: '/home', pathMatch: 'full'}
    ]
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: '**', component: DefaultLayoutComponent
  }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
